package id.corechain

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mashape.unirest.http.Unirest
import org.xml.sax.InputSource
import spark.Spark
import java.io.StringReader
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.soap.Node

private val bankTransfer = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"https://directchannel.cimbniaga.co.id:8002\" \n" +
        "xmlns:java=\"java:prismagateway.service.HostCustomer\">" +
        "   <soapenv:Header/>" +
        "   <soapenv:Body>" +
        "       <ns:HostCustomer>" +
        "           <ns:input>\n" +
        "               <java:tokenAuth>0314901bbe6790444622494b171a86c11de1a7bb6e014cbf07f7a4809664b716</java:tokenAuth>\n" +
        "               <java:txnData><![CDATA[\n" +
        "                   <transferRequest>\n" +
        "                   <transfer>\n" +
        "                       <transferId>%s</transferId>\n" +
        "                       <txnDate>%s</txnDate>\n" +
        "                       <debitAcctNo>%s</debitAcctNo>\n" +
        "                       <benAcctNo>%s</benAcctNo>\n" +
        "                       <benName>%s</benName>\n" +
        "                       <benAddr1>Alamat 1</benAddr1>\n" +
        "                       <benAddr2>Alamat 2</benAddr2>\n" +
        "                       <benBankCode>%s</benBankCode>\n" +
        "                       <benBankRTGS></benBankRTGS>\n" +
        "                       <benBankName>%s</benBankName>\n" +
        "                       <benBankAddr></benBankAddr>\n" +
        "                       <benBankCity>JAKARTA</benBankCity>\n" +
        "                       <benBankCountry>INDONESIA</benBankCountry>\n" +
        "                       <currAmount>IDR</currAmount>\n" +
        "                       <amount>%s</amount>\n" +
        "                       <memo>ATM_TRANSFER</memo>\n" +
        "                       <instructDate>%s</instructDate>\n" +
        "                       <remitID>%s</remitID>\n" +
        "                       <remitName>%s</remitName>\n" +
        "                       <remitLoc></remitLoc>\n" +
        "                   </transfer>\n" +
        "                   <charge>\n" +
        "                   <debitBankCharge></debitBankCharge>\n" +
        "                   <debitAgentCharge></debitAgentCharge>\n" +
        "                   </charge>\n" +
        "                   <filler>\n" +
        "                       <filler1></filler1>\n" +
        "                       <filler2></filler2>\n" +
        "                       <filler3></filler3>\n" +
        "                   </filler>\n" +
        "                   <lld>\n" +
        "                       <isLLD></isLLD>\n" +
        "                       <purpose></purpose>\n" +
        "                       <benStatus>02</benStatus>\n" +
        "                       <benCategory>1</benCategory>\n" +
        "                       <remStatus></remStatus>\n" +
        "                       <remCategory></remCategory>\n" +
        "                       <transRel></transRel>\n" +
        "                   </lld>\n" +
        "                   <advice>\n" +
        "                       <beneficiaryEmail></beneficiaryEmail>\n" +
        "                       <remitterEmail></remitterEmail>\n" +
        "                       <adviceDetail></adviceDetail>\n" +
        "                   </advice>\n" +
        "                   </transferRequest>]]>\n" +
        "                   </java:txnData><java:serviceCode>ATM_TRANSFER</java:serviceCode><java:corpID>%s</java:corpID>\n" +
        "                   <java:requestID>1862619247</java:requestID><java:txnRequestDateTime>%s</java:txnRequestDateTime>" +
        "           </ns:input>\n" +
        "       </ns:HostCustomer>" +
        "   </soapenv:Body>" +
        "</soapenv:Envelope>"

private val accountTransfer = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://10.25.143.133\" xmlns:java=\"java:prismagateway.service.HostCustomer\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "       <ns:HostCustomer>\n" +
        "           <ns:input>\n" +
        "               <java:tokenAuth>9fa0e2bf999bad32abe9304362ccd5fc56e9e5c2bda8dd081a048299f71e4143</java:tokenAuth>\n" +
        "               <java:txnData>\n" +
        "               <![CDATA[\n" +
        "                   <transferRequest>\n" +
        "                       <transfer>\n" +
        "                           <transferId>%s</transferId>\n" +
        "                           <txnDate>%s</txnDate>\n" +
        "                           <debitAcctNo>%s</debitAcctNo>\n" +
        "                           <benAcctNo>%s</benAcctNo>\n" +
        "                           <benName>%s</benName>\n" +
        "                           <benBankName>CIMB Niaga</benBankName>\n" +
        "                           <benBankAddr1></benBankAddr1>\n" +
        "                           <benBankAddr2></benBankAddr2>\n" +
        "                           <benBankAddr3></benBankAddr3>\n" +
        "                           <benBankBranch></benBankBranch>\n" +
        "                           <benBankCode>022</benBankCode>\n" +
        "                           <benBankSWIFT></benBankSWIFT>\n" +
        "                           <currCd>IDR</currCd>\n" +
        "                           <amount>%s</amount>\n" +
        "                           <memo>ACCOUNT TRANSFER TEST</memo>\n" +
        "                       </transfer>\n" +
        "                   </transferRequest>\n" +
        "               ]]>\n" +
        "               </java:txnData>\n" +
        "               <java:serviceCode>ACCOUNT_TRANSFER</java:serviceCode>\n" +
        "               <java:corpID>%s</java:corpID>\n" +
        "               <java:requestID>1767111607</java:requestID>\n" +
        "               <java:txnRequestDateTime>%s</java:txnRequestDateTime>\n" +
        "           </ns:input>\n" +
        "       </ns:HostCustomer>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>"

private val accountBalance = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"https://directchannel.cimbniaga.co.id:8002\" \n" +
        "xmlns:java=\"java:prismagateway.service.HostCustomer\">" +
        "   <soapenv:Header/>" +
        "   <soapenv:Body>" +
        "       <ns:HostCustomer>" +
        "           <ns:input>\n" +
        "               <java:tokenAuth>0314901bbe6790444622494b171a86c11de1a7bb6e014cbf07f7a4809664b716</java:tokenAuth>\n" +
        "               <java:txnData><![CDATA[" +
        "               <balanceRequest><balance><accountNo>%s</accountNo></balance></balanceRequest>" +
        "               ]]></java:txnData>" +
        "               <java:serviceCode>ACCOUNT_BALANCE</java:serviceCode>" +
        "   <java:corpID>%s</java:corpID><java:requestID>1705547510</java:requestID>" +
        "   <java:txnRequestDateTime>%s</java:txnRequestDateTime></ns:input></ns:HostCustomer>" +
        "   </soapenv:Body></soapenv:Envelope>"

fun main(args: Array<String>) {
    Spark.port(7063)

    val corpId = "ID9999BIZSLO"

    Spark.post("/digiroin/cashout/va/cimb") { req, res ->
        var request:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        try{
            json = parser.parse(request) as JsonObject
            try{
                var accountFrom =json.get("AccountFrom").asString
                var accountTo =json.get("AccountTo").asString
                var accountToName = json.get("AccountToName").asString
                var amount = json.get("Amount").asString
                if(accountFrom=="" || accountTo=="" || accountToName=="" || amount==""){
                    res.status(400)
                    return@post "{ \"status\":\"invalid parameter\"}"
                }
                val now = Date()
                val sdf = SimpleDateFormat("yyyyMMdd")
                val sdf2 = SimpleDateFormat("yyyyMMddHHmmss")
                val body = String.format(accountTransfer, now.time,sdf.format(now),accountFrom,accountTo,accountToName,amount,corpId,sdf2.format(now))
                val jsonResponse = Unirest.post("http://pguat.cimbniaga.co.id:8004/PrismaGateway/services/HostCustomer").header("Content-Type","text/xml").header("SOAPAction","")
                        .body(body).asString()
                val factory = DocumentBuilderFactory.newInstance()
                val builder = factory.newDocumentBuilder()
                val document = builder.parse(InputSource(StringReader(jsonResponse.body)))
                val n = document.firstChild
                val nl = n.childNodes
                var an: org.w3c.dom.Node
                var an2: org.w3c.dom.Node
                var an3: org.w3c.dom.Node
                try{
                    val tagNode = HashMap<String, String>()
                    for (i in 0..nl.length - 1) {
                        an = nl.item(i)
                        if (an.nodeType == Node.ELEMENT_NODE) {
                            val nl2 = an.childNodes
                            for (i2 in 0..nl2.length - 1) {
                                an2 = nl2.item(i2)
                                val nl3 = an2.childNodes
                                for (i3 in 0..nl3.length - 1) {
                                    an3 = nl3.item(i3)
                                    var sibling: org.w3c.dom.Node? = an3.firstChild
                                    //input first child node
                                    if (sibling != null) tagNode.put(sibling.nodeName, sibling.textContent)
                                    while (sibling != null) {
                                        try {
                                            //iterate next child
                                            sibling = sibling.nextSibling
                                            tagNode.put(sibling!!.nodeName, sibling.textContent)
                                        } catch (e: NullPointerException) {
                                            continue
                                        }
                                    }
                                }
                            }
                        }
                    }
                    val code = tagNode.get("ns2:statusCode")
                    val message = tagNode.get("ns2:statusMsg")
                    val data = tagNode.get("ns2:txnData")
                    if(code=="00"){
                        try{
                            var clearData = data!!.replace("\\s".toRegex(), "")
                            val factory = DocumentBuilderFactory.newInstance()
                            val builder = factory.newDocumentBuilder()
                            val document = builder.parse(InputSource(StringReader(clearData)))
                            val n = document.firstChild
                            val nl = n.childNodes
                            var an: org.w3c.dom.Node
                            var an2: org.w3c.dom.Node
                            val tagNode2 = HashMap<String, String>()
                            for (i in 0..nl.length - 1) {
                                an = nl.item(i)
                                if (an.nodeType == Node.ELEMENT_NODE) {
                                    val nl2 = an.childNodes
                                    for (i2 in 0..nl2.length - 1) {
                                        an2 = nl2.item(i2)
                                        var value: org.w3c.dom.Node? = an2.previousSibling
                                        var key: org.w3c.dom.Node? = an2.previousSibling
                                        if (value != null && key!=null) {
                                            tagNode2.put(key.nodeName,value.textContent)
                                        }
                                    }
                                }
                            }
                            var date = tagNode2.get("txnDate")
                            println(tagNode2.get("debitAcctNo"))
                            println(tagNode2.get("benAcctNo"))
                            println(tagNode2.get("benName"))
                            println(tagNode2.get("benBankName"))
                            println(tagNode2.get("benBankCode"))
                            println(date)
                            println(tagNode2.get("currCd"))
                            println(tagNode2.get("amount"))
                            println(Date().time)
                            return@post "{ \"status\":\"cashout success\"}"
                        }catch (e:Exception){
                            res.status(500)
                            return@post "{ \"status\":\""+e.message+"\"}"
                        }
                    }else{
                        res.status(500)
                        return@post "{ \"status\":\""+message+"\"}"
                    }
                }catch (e:Exception){
                    res.status(500)
                    return@post "{ \"status\":\""+e.message+"\"}"
                }
            }catch (e:Exception){
                res.status(400)
                return@post "{ \"status\":\"invalid json\"}"
            }
        }catch (e:Exception){
            res.status(500)
            return@post "{ \"status\":\""+e.message+"\"}"
        }
    }

    Spark.post("/digiroin/cashout/other/va/cimb") { req, res ->
        var request:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        try{
            json = parser.parse(request) as JsonObject
            try{
                var accountFrom =json.get("AccountFrom").asString
                var accountTo =json.get("AccountTo").asString
                var accountToName = json.get("AccountToName").asString
                var amount = json.get("Amount").asString
                var accountToBankCode = json.get("AccountToBankCode").asString
                var accountToBankName = json.get("AccountToBankName").asString
                if(accountFrom=="" || accountTo=="" || accountToName=="" || amount==""){
                    res.status(400)
                    return@post "{ \"status\":\"invalid parameter\"}"
                }
                val now = Date()
                val sdf = SimpleDateFormat("yyyyMMdd")
                val sdf2 = SimpleDateFormat("yyyyMMddHHmmss")
                val body = String.format(bankTransfer, now.time,sdf.format(now),accountFrom,accountTo,accountToName,accountToBankCode,accountToBankName,amount,sdf.format(now),now.time,accountToName,corpId,sdf2.format(now))
                val jsonResponse = Unirest.post("http://pguat.cimbniaga.co.id:8004/PrismaGateway/services/HostCustomer").header("Content-Type","text/xml").header("SOAPAction","")
                        .body(body).asString()
                val factory = DocumentBuilderFactory.newInstance()
                val builder = factory.newDocumentBuilder()
                val document = builder.parse(InputSource(StringReader(jsonResponse.body)))
                val n = document.firstChild
                val nl = n.childNodes
                var an: org.w3c.dom.Node
                var an2: org.w3c.dom.Node
                var an3: org.w3c.dom.Node
                try{
                    val tagNode = HashMap<String, String>()
                    for (i in 0..nl.length - 1) {
                        an = nl.item(i)
                        if (an.nodeType == Node.ELEMENT_NODE) {
                            val nl2 = an.childNodes
                            for (i2 in 0..nl2.length - 1) {
                                an2 = nl2.item(i2)
                                val nl3 = an2.childNodes
                                for (i3 in 0..nl3.length - 1) {
                                    an3 = nl3.item(i3)
                                    var sibling: org.w3c.dom.Node? = an3.firstChild
                                    //input first child node
                                    if (sibling != null) tagNode.put(sibling!!.nodeName, sibling!!.textContent)
                                    while (sibling != null) {
                                        try {
                                            //iterate next child
                                            sibling = sibling!!.nextSibling
                                            tagNode.put(sibling!!.nodeName, sibling!!.textContent)
                                        } catch (e: NullPointerException) {
                                            continue
                                        }
                                    }
                                }
                            }
                        }
                    }
                    val code = tagNode.get("ns2:statusCode")
                    val message = tagNode.get("ns2:statusMsg")
                    val data = tagNode.get("ns2:txnData")
                    if(code=="00"){
                        try{
                            var clearData = data!!.replace("\\s".toRegex(), "")
                            val factory = DocumentBuilderFactory.newInstance()
                            val builder = factory.newDocumentBuilder()
                            val document = builder.parse(InputSource(StringReader(clearData)))
                            val n = document.firstChild
                            val nl = n.childNodes
                            var an: org.w3c.dom.Node
                            var an2: org.w3c.dom.Node
                            val tagNode2 = HashMap<String, String>()
                            for (i in 0..nl.length - 1) {
                                an = nl.item(i)
                                if (an.nodeType == Node.ELEMENT_NODE) {
                                    val nl2 = an.childNodes
                                    for (i2 in 0..nl2.length - 1) {
                                        an2 = nl2.item(i2)
                                        var value: org.w3c.dom.Node? = an2.previousSibling
                                        var key: org.w3c.dom.Node? = an2.previousSibling
                                        if (value != null && key!=null) {
                                            tagNode2.put(key.nodeName,value.textContent)
                                        }
                                    }
                                }
                            }
                            var date = tagNode2.get("txnDate")
                            println(tagNode2.get("debitAcctNo"))
                            println(tagNode2.get("benAcctNo"))
                            println(tagNode2.get("benName"))
                            println(tagNode2.get("benBankCode"))
                            println(date)
                            println(tagNode2.get("currCd"))
                            println(tagNode2.get("amount"))
                            println(Date().time)
                            return@post "{ \"status\":\"cashout success\"}"
                        }catch (e:Exception){
                            res.status(500)
                            return@post "{ \"status\":\""+e.message+"\"}"
                        }
                    }else{
                        res.status(500)
                        return@post "{ \"status\":\""+message+"\"}"
                    }
                }catch (e:Exception){
                    res.status(500)
                    return@post "{ \"status\":\""+e.message+"\"}"
                }
            }catch (e:Exception){
                res.status(400)
                return@post "{ \"status\":\"invalid json\"}"
            }
        }catch (e:Exception){
            res.status(500)
            return@post "{ \"status\":\""+e.message+"\"}"
        }
    }

    Spark.post("/digiroin/balance/va/cimb") { req, res ->
        var request:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        try{
            json = parser.parse(request) as JsonObject
            try{
                var account =json.get("AccountNumber").asString
                if(account==""){
                    res.status(400)
                    return@post "{ \"status\":\"invalid parameter\"}"
                }
                val now = Date()
                val sdf2 = SimpleDateFormat("yyyyMMddHHmmss")
                val body = String.format(accountBalance,account,corpId,sdf2.format(now))
                val jsonResponse = Unirest.post("http://pguat.cimbniaga.co.id:8004/PrismaGateway/services/HostCustomer").header("Content-Type","text/xml").header("SOAPAction","")
                        .body(body).asString()
                print(jsonResponse.body)
                val factory = DocumentBuilderFactory.newInstance()
                val builder = factory.newDocumentBuilder()
                val document = builder.parse(InputSource(StringReader(jsonResponse.body)))
                val n = document.firstChild
                val nl = n.childNodes
                var an: org.w3c.dom.Node
                var an2: org.w3c.dom.Node
                var an3: org.w3c.dom.Node
                try{
                    val tagNode = HashMap<String, String>()
                    for (i in 0..nl.length - 1) {
                        an = nl.item(i)
                        if (an.nodeType == Node.ELEMENT_NODE) {
                            val nl2 = an.childNodes
                            for (i2 in 0..nl2.length - 1) {
                                an2 = nl2.item(i2)
                                val nl3 = an2.childNodes
                                for (i3 in 0..nl3.length - 1) {
                                    an3 = nl3.item(i3)
                                    var sibling: org.w3c.dom.Node? = an3.firstChild
                                    //input first child node
                                    if (sibling != null) tagNode.put(sibling!!.nodeName, sibling!!.textContent)
                                    while (sibling != null) {
                                        try {
                                            //iterate next child
                                            sibling = sibling!!.nextSibling
                                            tagNode.put(sibling!!.nodeName, sibling!!.textContent)
                                        } catch (e: NullPointerException) {
                                            continue
                                        }
                                    }
                                }
                            }
                        }
                    }
                    val code = tagNode.get("ns2:statusCode")
                    val message = tagNode.get("ns2:statusMsg")
                    val data = tagNode.get("ns2:txnData")
                    if(code=="00"){
                        try{
                            var clearData = data!!.replace("\\s".toRegex(), "")
                            val factory = DocumentBuilderFactory.newInstance()
                            val builder = factory.newDocumentBuilder()
                            val document = builder.parse(InputSource(StringReader(clearData)))
                            val n = document.firstChild
                            val nl = n.childNodes
                            var an: org.w3c.dom.Node
                            var an2: org.w3c.dom.Node
                            val tagNode2 = HashMap<String, String>()
                            for (i in 0..nl.length - 1) {
                                an = nl.item(i)
                                if (an.nodeType == Node.ELEMENT_NODE) {
                                    val nl2 = an.childNodes
                                    for (i2 in 0..nl2.length - 1) {
                                        an2 = nl2.item(i2)
                                        var value: org.w3c.dom.Node? = an2.previousSibling
                                        var key: org.w3c.dom.Node? = an2.previousSibling
                                        if (value != null && key!=null) {
                                            tagNode2.put(key.nodeName,value.textContent)
                                        }
                                    }
                                }
                            }
                            var date = tagNode2.get("txnDate")
                            println(tagNode2.get("accountNo"))
                            println(tagNode2.get("date"))
                            println(tagNode2.get("availableBalance"))
                            return@post "{ \"status\":\"ok\",\"balance\":\""+tagNode2.get("availableBalance")+"\"}"
                        }catch (e:Exception){
                            res.status(500)
                            return@post "{ \"status\":\""+e.message+"\"}"
                        }
                    }else{
                        res.status(500)
                        return@post "{ \"status\":\""+message+"\"}"
                    }
                }catch (e:Exception){
                    res.status(500)
                    return@post "{ \"status\":\""+e.message+"\"}"
                }
            }catch (e:Exception){
                e.printStackTrace()
                res.status(400)
                return@post "{ \"status\":\"invalid json\"}"
            }
        }catch (e:Exception){
            res.status(500)
            return@post "{ \"status\":\""+e.message+"\"}"
        }
    }
}
